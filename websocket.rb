#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# How to invoke this program with Java system property 'ssl' set to true
# without root privileges:
#
#     jruby websocket.rb --ssl --port=4443
#
# You may now browse to https://localhost:4443/ to test.
#
#
# How to have this program listen to standard ssl port 443 (this
# typically requires root privileges):
#
#     sudo jruby websocket.rb --ssl
#
#
# How to have this program listen to standard ssl port 443 and use
# a certificate and private key from the local file system:
#
#     sudo jruby websocket.rb --ssl \
#       --ssl-certificate=/etc/letsencrypt/live/example.com/fullchain.pem \
#       --ssl-private-key=/etc/letsencrypt/live/example.com/privkey.pem
#
# You may now browse to https://localhost/ to test.
#

require_relative 'lib/websocket_server'

Object.new.extend(WebSocket).main
