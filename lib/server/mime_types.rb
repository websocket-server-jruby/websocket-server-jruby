# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# The Server module
module Server
  MimeTypes = {
    txt: 'text/plain',
    css: 'text/css',
    csv: 'text/csv',
    htm: 'text/html',
    html: 'text/html',
    xml: 'text/xml',
    js: 'application/javascript',
    xhtml: 'application/xhtml+xml',
    json: 'application/json',
    pdf: 'application/pdf',
    woff: 'application/x-font-woff',
    zip: 'application/zip',
    tar: 'application/x-tar',
    gif: 'image/gif',
    jpeg: 'image/jpeg',
    jpg: 'image/jpeg',
    tiff: 'image/tiff',
    tif: 'image/tiff',
    png: 'image/png',
    svg: 'image/svg+xml',
    ico: 'image/vnd.microsoft.icon'
  }.freeze
end
