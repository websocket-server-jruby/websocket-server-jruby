#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'optparse'

require 'netty'

require_relative 'logging'

# Simple websocket client ported from the example from Netty-io.

# The WebSocket module
module WebSocket
  # rubocop: disable Metrics/AbcSize
  def client_config
    @client_config ||= {
      log_level: Logger::INFO,
      uri: java.net.URI.new('ws://127.0.0.1:4000/websocket'),
      # Connect with V13 (RFC 6455 aka HyBi-17). You can change it to V08 or V00.
      # If you change it to V00, ping is not supported and remember to change
      # HttpResponseDecoder to WebSocketHttpResponseDecoder in the pipeline.
      websocket_version: Java::io.netty.handler.codec.http.websocketx.WebSocketVersion::V13,
      subprotocol: nil,
      allow_extensions: true,
      default_headers: Java::io.netty.handler.codec.http.DefaultHttpHeaders.new,
      prompt: '>'
    }
  end
  module_function :client_config
  # rubocop: enable Metrics/AbcSize
end
# module WebSocket

# The WebSocket module
module WebSocket
  SUPPORTED_SCHEMES = %w[ws wss].freeze
  CHANNEL_TYPE = Java::io.netty.channel.socket.nio.NioSocketChannel.java_class
  PING_MSG_CONTENT = [8, 1, 8, 1].to_java(:byte)
  UNEXPECTED_FULL_RESPONSE_ERROR =
    'Unexpected FullHttpResponse (getStatus=%<status>s, content=%<content>s)'.freeze
end
# module WebSocket

# The WebSocket module
module WebSocket
  java_import Java::io.netty.bootstrap.Bootstrap
  java_import Java::io.netty.channel.nio.NioEventLoopGroup
  java_import Java::io.netty.handler.ssl.SslContext
  java_import Java::io.netty.handler.ssl.SslContextBuilder
  java_import Java::io.netty.handler.ssl.util.InsecureTrustManagerFactory

  # The ClientInitializationMethods module
  module ClientInitializationMethods
    def init(options)
      @options = options
      configure_from_uri
      @queue = java.util.concurrent.LinkedBlockingQueue.new
    end

    def bootstrap
      @bootstrap = Bootstrap.new
      @bootstrap.group(client_group)
      @bootstrap.channel(::WebSocket::CHANNEL_TYPE)
      @bootstrap.handler(logging_handler) if @options[:log_requests]
      @bootstrap.handler(channel_initializer)
    end

    def client_group
      @client_group ||= NioEventLoopGroup.new
    end

    def logging_handler
      @logging_handler ||= LoggingHandler.new(LogLevel::INFO)
    end

    def channel_initializer
      @channel_initializer ||= ::WebSocket::ClientChannelInitializer.new(@host, @port, @scheme, @options)
    end

    def validate_scheme(scheme)
      raise 'Only WS(S) is supported' unless ::WebSocket::SUPPORTED_SCHEMES.include?(scheme)
    end

    def configure_from_uri
      @uri = @options[:uri]
      @scheme = validate_scheme(@uri.getScheme()&.downcase || 'ws')
      @host = @uri.getHost() || '127.0.0.1'
      @port = @uri.getPort() || case @scheme
      when /ws/i then 80
      when /wss/i then 443
      else -1
      end
    end

    def configure_handlers(*handlers, &block)
      channel_initializer.default_handler.add_listener(self)
      channel_initializer.default_handler.listeners.addAll(handlers)
      @user_app = block
      @application_handler = lambda do |ctx, msg|
        if @user_app.nil? || @user_app.arity == 1
          @queue.add(msg.chomp)
        else
          @user_app.call(ctx, msg)
        end
      end
    end
  end
  # module ClientInitializationMethods
end
# module WebSocket

# The WebSocket module
module WebSocket
  java_import Java::io.netty.buffer.Unpooled
  java_import Java::io.netty.channel.AbstractChannel
  java_import Java::io.netty.handler.codec.http.websocketx.CloseWebSocketFrame
  java_import Java::io.netty.handler.codec.http.websocketx.PingWebSocketFrame
  java_import Java::io.netty.handler.codec.http.websocketx.PongWebSocketFrame
  java_import Java::io.netty.handler.codec.http.websocketx.TextWebSocketFrame

  # The ClientInstanceMethods module
  module ClientInstanceMethods
    def puts(msg)
      sleep 0.1 until @channel.isActive()
      msg.chomp!
      log.trace "#puts msg: #{msg.inspect}"
      raise 'Message is empty!' if msg.nil? || msg.empty?
      @last_write_future = @channel.writeAndFlush(TextWebSocketFrame.new(msg))
    end

    def gets
      log.debug 'Waiting for response from server'
      @queue.take
    rescue StandardError => e
      warn "Unexpected error waiting for message: #{e.message}"
      nil
    end

    def connect
      @channel = bootstrap.connect(@host, @port).sync().channel()
      channel_initializer.default_handler.handshake_future.sync()
    rescue AbstractChannel::AnnotatedConnectException => e
      raise e.message
    rescue StandardError => e
      raise "Connection failure: #{e.message}"
    end

    def close(channel = @channel)
      log.debug 'Closing primary channel'
      channel.writeAndFlush(CloseWebSocketFrame.new)
      channel.closeFuture().sync()
    ensure
      shutdown
    end

    def shutdown
      log.debug 'Shutting down gracefully'
      @client_group&.shutdownGracefully()
    ensure
      client_has_shut_down
    end

    def ping
      @channel.writeAndFlush(PingWebSocketFrame.new(Unpooled.wrappedBuffer(PING_MSG_CONTENT)))
    end

    def session
      when_client_has_shut_down(@client_group) do |group|
        log.debug "Channel group has shut down: #{group.inspect}"
      end
      @user_app.nil? ? read_user_commands : invoke_user_app
    end

    def invoke_user_app
      @user_app&.call(self)
    ensure
      close
    end

    def shut_down_callbacks
      @shut_down_callbacks ||= []
    end

    def when_client_has_shut_down(*args, &block)
      shut_down_callbacks << {
        block: block,
        args: args
      }
      self
    end

    def client_has_shut_down
      shut_down_callbacks.take_while do |callback|
        callback[:block]&.call(*callback.fetch(:args, []))
      end
    rescue StandardError => e
      log.error e.message
    end

    def channelActive(ctx)
      log.info "Connected to #{ctx.channel().remoteAddress0()}"
    end

    def channelInactive(ctx)
      log.info "Disconnected from #{ctx.channel().remoteAddress0()}"
    end

    def messageReceived(ctx, msg)
      log.trace "Received message: #{msg}"
      case msg
      when TextWebSocketFrame then @application_handler.call(ctx, msg.text())
      when PongWebSocketFrame then puts 'pong!'
      when CloseWebSocketFrame then ctx.channel().close()
      end
    end

    def execute_command(str, websocket = self)
      return if str.empty?
      case str
      when /bye/i, /quit/i
        websocket.puts "#{str}\r\n"
        close
      when /ping/i then ping
      else websocket.puts "#{str}\r\n"
      end
    end

    def read_user_commands(websocket = self)
      loop do
        print @options[:prompt]
        input = $stdin.gets.chomp
        raise 'Poll failure from stdin' if input.nil?
        break unless @channel.active?
        break if execute_command(input).is_a?(AbstractChannel::CloseFuture)
        $stdout.puts websocket.gets
      end
    end
  end
  # module ClientInstanceMethods
end
# module WebSocket

# The WebSocket module
module WebSocket
  # The Listenable module
  module Listenable
    def listeners
      @listeners ||= java.util.concurrent.CopyOnWriteArrayList.new
    end

    def add_listener(*listener)
      listeners.addAll(listener)
    end

    def remove_listener(*listener)
      listeners.removeAll(listener)
    end

    def replace_listeners(*listener)
      listeners.clear
      add_listener(*listener)
    end

    def notify(event, *args)
      listeners.each do |listener|
        next unless listener.respond_to?(event.to_sym)
        log.trace "Notifying listener #{listener} of event: #{event}"
        listener.send(event.to_sym, *args)
      end
    end
  end
  # module Listenable
end
# module WebSocket

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.ChannelHandlerContext
  java_import Java::io.netty.channel.SimpleChannelInboundHandler
  java_import Java::io.netty.handler.codec.http.FullHttpResponse
  java_import Java::io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker
  java_import Java::io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory
  java_import Java::io.netty.handler.codec.http.websocketx.WebSocketHandshakeException
  java_import Java::io.netty.util.CharsetUtil

  # The ClientChannelHandler class
  class ClientChannelHandler < SimpleChannelInboundHandler
    include Listenable
    attr_reader :handshake_future

    def initialize(handshaker)
      super()
      @handshaker = handshaker
    end

    def handlerAdded(ctx)
      @handshake_future = ctx.newPromise()
    end

    def channelActive(ctx)
      @handshaker.handshake(ctx.channel())
      notify(:channelActive, ctx)
    end

    def channelInactive(ctx)
      notify(:channelInactive, ctx)
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # java_signature 'protected void channelRead0(ChannelHandlerContext ctx, I msg) throws Exception'
    def channelRead0(ctx, msg)
      messageReceived(ctx, msg)
    end

    def messageReceived(ctx, msg)
      return finish_handshake(ctx, msg) unless @handshaker.isHandshakeComplete()
      validate_message(msg)
      notify(:messageReceived, ctx, msg)
    end

    def exceptionCaught(ctx, cause)
      cause.printStackTrace()
      @handshake_future.setFailure(cause) unless @handshake_future.isDone()
      ctx.close()
    end

    protected

    def finish_handshake(ctx, msg)
      @handshaker.finishHandshake(ctx.channel(), msg)
      @handshake_future.setSuccess()
    rescue WebSocketHandshakeException => e
      log.warn "Connection failure: #{e.message}"
      @handshake_future.setFailure(e)
    end

    def validate_message(msg)
      case msg
      when FullHttpResponse
        raise IllegalStateException, format(
          UNEXPECTED_FULL_RESPONSE_ERROR,
          status: msg.status(),
          content: msg.content().toString(CharsetUtil::UTF_8)
        )
      end
    end
  end
  # class ClientChannelHandler
end
# module WebSocket

# The WebSocket module
module WebSocket
  java_import Java::io.netty.handler.codec.http.HttpClientCodec
  java_import Java::io.netty.handler.codec.http.HttpObjectAggregator
  java_import Java::io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketClientCompressionHandler

  # The ClientChannelInitializer class
  class ClientChannelInitializer < Java::io.netty.channel.ChannelInitializer
    FrameDecoderBufferSize = 8192 # bytes
    def initialize(host, port, scheme, options = {})
      super()
      @host = host
      @port = port
      @options = options
      @ssl_ctx = ssl_context if /wss/i.match?(scheme)
    end

    def handshaker
      @handshaker ||= WebSocketClientHandshakerFactory.newHandshaker(
        @options[:uri],
        @options[:websocket_version],
        @options[:subprotocol],
        @options[:allow_extensions],
        @options[:default_headers]
      )
    end

    def default_handler
      @default_handler ||= ::WebSocket::ClientChannelHandler.new(handshaker)
    end

    def ssl_context
      context_builder = SslContextBuilder.forClient()
      trust_manager = context_builder.trustManager(InsecureTrustManagerFactory::INSTANCE)
      trust_manager.build()
    end

    def initChannel(channel)
      pipeline = channel.pipeline()
      pipeline.addLast(@ssl_ctx.newHandler(channel.alloc(), @host, @port)) unless @ssl_ctx.nil?
      pipeline.addLast(
        HttpClientCodec.new,
        HttpObjectAggregator.new(FrameDecoderBufferSize),
        WebSocketClientCompressionHandler::INSTANCE
      )
      pipeline.addLast(default_handler)
    end
  end
  # class ClientChannelInitializer
end
# module WebSocket

# The WebSocket module
module WebSocket
  # The WebSocket::Client class
  class Client
    include ::WebSocket::ClientInitializationMethods
    include ::WebSocket::ClientInstanceMethods
    include ::WebSocket::Listenable

    def initialize(options = {}, &block)
      init(::WebSocket.client_config.merge(options))
      configure_handlers(&block)
      connect
      session
    end

    def to_s
      format('#<%<class>s:0x%<id>s>', class: self.class.name, id: object_id.to_s(16))
    end
    alias inspect to_s
  end
end
# module WebSocket

# The WebSocket module
module WebSocket
  # The ArgumentsParser class
  class ArgumentsParser
    attr_reader :parser, :options

    def initialize(parser = OptionParser.new, options = ::WebSocket.client_config.dup)
      @parser = parser
      @options = options
      @flags = %i[banner uri log_level help version]
      @flags.each { |method_name| method(method_name)&.call if respond_to?(method_name) }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def uri
      @parser.on_head('-u', '--uri=<uri>', 'Fully qualified connection string') do |v|
        @options[:uri] = java.net.URI.new(v)
      end
    end

    def log_level
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] ||= 0
        @options[:log_level] -= 1
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{::WebSocket::VERSION}"
        exit
      end
    end
  end
  # class ArgumentsParser

  def parse_arguments(arguments_parser = ArgumentsParser.new)
    arguments_parser.parser.parse!(ARGV)
    arguments_parser.parse_positional_arguments!
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
    puts e.message
    puts parser
    exit
  rescue OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module WebSocket

# The WebSocket module
module WebSocket
  def main(args = parse_arguments)
    Logging.log_level = args[:log_level]
    ::WebSocket::Client.new(args)
  rescue Interrupt => e
    warn "\n#{e.class}"
    exit
  rescue StandardError => e
    ::WebSocket::Client.log.fatal "Unexpected error: #{e.class}: #{e.message}"
    e.backtrace.each { |t| log.debug t } if Logging.log_level == Logger::DEBUG
    exit 1
  end
end
# module WebSocket

Object.new.extend(WebSocket).main if $PROGRAM_NAME == __FILE__
