# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'logger'

# The WebSocket module
module WebSocket
  WEBSOCKET_SUBMODULE_DIR_PATH = File.expand_path(__dir__)
  LIB_DIR_PATH = File.expand_path(File.dirname(WEBSOCKET_SUBMODULE_DIR_PATH))
  PROJECT_DIR_PATH = File.expand_path(File.dirname(LIB_DIR_PATH))
  DEFAULT_LOG_LEVEL = Logger::INFO
  DEFAULT_HOSTNAME = '0.0.0.0'.freeze
  DEFAULT_PORT = 4000
  DEFAULT_SSL_PORT = 443
  DEFAULT_TELNET_PROXY_HOST = nil
  DEFAULT_TELNET_PROXY_PORT = 21
  DEFAULT_KEEP_ALIVE = true
  DEFAULT_MAX_QUEUED_INCOMING_CONNECTIONS = 100
  DEFAULT_IDLE_READING_SECONDS = 5 * 60 # seconds
  DEFAULT_IDLE_WRITING_SECONDS = 30 # seconds
  DEFAULT_SSL_ENABLED = false
  DEFAULT_SSL_CERTIFICATE_FILE_PATH = File.join(PROJECT_DIR_PATH, 'fullchain.pem')
  # openssl pkcs8 -topk8 -nocrypt -in websocket.key -out websocket_pcks8
  #
  # The privkey.key file here should be generated using the following
  # openssl command:
  #
  #    openssl pkcs8 -topk8 -inform PEM -outform DER -in privkey.pem -nocrypt > privkey.key
  #
  DEFAULT_SSL_PRIVATE_KEY_FILE_PATH = File.join(PROJECT_DIR_PATH, 'privkey.key')
  DEFAULT_SSL_INSPECTION_ENABLED = false
  DEFAULT_WEB_SOCKET_PATH = '/websocket'.freeze
  DEFAULT_WEB_DIR = 'web'.freeze
  DEFAULT_WEB_ROOT = File.join(PROJECT_DIR_PATH, DEFAULT_WEB_DIR)
  DEFAULT_INDEX_PAGE = 'index.html'.freeze
  DEFAULT_PING_MESSAGE = "ping\n".freeze
  # EEE, dd MMM yyyy HH:mm:ss zzz
  DEFAULT_HTTP_DATE_FORMAT = '%a, %d %b %Y %H:%M:%S %Z'.freeze
  DEFAULT_HTTP_DATE_GMT_TIMEZONE = 'GMT'.freeze
  DEFAULT_CACHE_SECONDS = 60 # seconds
  DEFAULT_INSECURE_URI_PATTERN = %r{.*[<>&"].*}
  DEFAULT_LOG_REQUESTS = false

  # rubocop: disable Metrics/MethodLength
  def server_config
    @server_config ||= {
      log_level: DEFAULT_LOG_LEVEL,
      host: DEFAULT_HOSTNAME,
      port: DEFAULT_PORT,
      ssl_port: DEFAULT_SSL_PORT,
      telnet_proxy_host: DEFAULT_TELNET_PROXY_HOST,
      telnet_proxy_port: DEFAULT_TELNET_PROXY_PORT,
      ssl: DEFAULT_SSL_ENABLED,
      ssl_certificate_file_path: DEFAULT_SSL_CERTIFICATE_FILE_PATH,
      ssl_private_key_file_path: DEFAULT_SSL_PRIVATE_KEY_FILE_PATH,
      use_jdk_ssl_provider: false,
      inspect_ssl: DEFAULT_SSL_INSPECTION_ENABLED,
      idle_reading: DEFAULT_IDLE_READING_SECONDS,
      idle_writing: DEFAULT_IDLE_WRITING_SECONDS,
      keep_alive: DEFAULT_KEEP_ALIVE,
      max_queued_incoming_connections: DEFAULT_MAX_QUEUED_INCOMING_CONNECTIONS,
      index_page: DEFAULT_INDEX_PAGE,
      web_root: DEFAULT_WEB_ROOT,
      web_socket_path: DEFAULT_WEB_SOCKET_PATH,
      ping_message: DEFAULT_PING_MESSAGE,
      http_date_format: DEFAULT_HTTP_DATE_FORMAT,
      http_date_gmt_timezone: DEFAULT_HTTP_DATE_GMT_TIMEZONE,
      http_cache_seconds: DEFAULT_CACHE_SECONDS,
      insecure_uri_pattern: DEFAULT_INSECURE_URI_PATTERN,
      log_requests: DEFAULT_LOG_REQUESTS
    }
  end
  module_function :server_config
  # rubocop: enable Metrics/MethodLength
end
