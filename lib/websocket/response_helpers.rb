# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'encoding'

# The WebSocket module
module WebSocket
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import Java::io.netty.buffer.Unpooled
  java_import Java::io.netty.handler.codec.http.DefaultFullHttpResponse
  java_import Java::io.netty.handler.codec.http.HttpHeaderNames
  java_import Java::io.netty.handler.codec.http.HttpVersion
  java_import Java::io.netty.util.CharsetUtil

  # The ResponseHelpers helpers
  module ResponseHelpers
    NON_SPACES_BEFORE_EOL_PATTERN = %r{([^\s]+)$}

    def index_listing(path)
      results = `ls -la #{path}`.strip.split("\n")
      results.shift
      index = results.collect do |s|
        s.gsub(NON_SPACES_BEFORE_EOL_PATTERN) do |resource_name|
          %(<a href="#{resource_name}">#{resource_name}</a>)
        end
      end.join '<br />'
      "<html><pre>#{index}</pre></html>"
    end

    def send_listing(ctx, path)
      response = DefaultFullHttpResponse.new(HttpVersion::HTTP_1_1, HttpResponseStatus::OK)
      response.headers().set(HttpHeaderNames::CONTENT_TYPE, WebSocket::HtmlContentType)

      html = index_listing(path)

      buffer = Unpooled.copiedBuffer(html, WebSocket::Encoding)
      response.content().writeBytes(buffer)
      buffer.release()

      # Close the connection as soon as the error message is sent.
      ctx.writeAndFlush(response).addListener(ChannelFutureListener::CLOSE)
    end

    def send_redirect(ctx, redirect_to_uri)
      response = DefaultFullHttpResponse.new(HttpVersion::HTTP_1_1, HttpResponseStatus::FOUND)
      response.headers().set(HttpHeaderNames::LOCATION, redirect_to_uri)

      # Close the connection as soon as the error message is sent.
      ctx.writeAndFlush(response).addListener(ChannelFutureListener::CLOSE)
    end

    def send_error(ctx, status)
      message = Unpooled.copiedBuffer("#{status}\r\n", CharsetUtil::UTF_8)
      response = DefaultFullHttpResponse.new(HttpVersion::HTTP_1_1, status, message)
      response.headers().set(HttpHeaderNames::CONTENT_TYPE, WebSocket::HtmlContentType)

      # Close the connection as soon as the error message is sent.
      ctx.writeAndFlush(response).addListener(ChannelFutureListener::CLOSE)
    end

    def send_not_modified(ctx, date)
      response = DefaultFullHttpResponse.new(HttpVersion::HTTP_1_1, HttpResponseStatus::NOT_MODIFIED)
      date_header(response, date)

      # Close the connection as soon as the error message is sent.
      ctx.writeAndFlush(response).addListener(ChannelFutureListener::CLOSE)
    end
  end
  # module ResponseHelpers
end
# module WebSocket
