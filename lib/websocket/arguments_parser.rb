# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'optparse'

require_relative 'config'

# The WebSocket module
module WebSocket
  # rubocop: disable Metrics/ClassLength
  # The ArgumentsParser class
  class ArgumentsParser
    attr_reader :parser, :options

    def initialize(option_parser = OptionParser.new)
      @parser = option_parser
      @options = ::WebSocket.server_config.dup
      @flags = %i[
        banner port telnet_proxy_host telnet_proxy_port
        ssl ssl_certificate ssl_private_key use_jdk_ssl_provider
        inspect_ssl idle_reading idle_writing log_requests web_root
        verbose help version
      ]
      @flags.each { |method_name| method(method_name)&.call if respond_to?(method_name) }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [port] [options]"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def validated_port(val, integer_pattern = /^\d+$/)
      raise OptionParser::InvalidArgument, "Invalid port: #{v}" unless \
        integer_pattern.match?(val.to_s) && val.positive? && val < 65_536

      val
    end

    def port
      description = "Port on which to listen for connections; default: #{@options[:port]}"
      @parser.on('-p', '--port=<port>', Integer, description) do |v|
        @options[:port] = validated_port(v).to_i
      end
    end

    def telnet_proxy_host
      @parser.on('--telnet-proxy-host=host', 'Remote telnet proxy host') do |v|
        @options[:telnet_proxy_host] = v
      end
    end

    def telnet_proxy_port
      @parser.on('--telnet-proxy-port=port', 'Remote telnet proxy port') do |v|
        @options[:telnet_proxy_port] = v.to_i
      end
    end

    def ssl
      @parser.on('-s', '--ssl', 'Use TLS/SSL') do |v|
        @options[:ssl] = v
      end
    end

    def ssl_certificate
      @parser.on('-c', '--ssl-certificate=file', 'Path to ssl certificate file') do |v|
        @options[:ssl_certificate_file_path] = v
      end
    end

    def ssl_private_key
      @parser.on('-k', '--ssl-private-key=file', 'Path to private key file') do |v|
        @options[:ssl_private_key_file_path] = v
      end
    end

    def use_jdk_ssl_provider
      @parser.on('--use-jdk-ssl-provider', 'Use the JDK SSL provider') do
        @options[:use_jdk_ssl_provider] = true
      end
    end

    def inspect_ssl
      @parser.on('--inspect-ssl', 'Verbosely log SSL messages for encryption confirmation') do
        @options[:inspect_ssl] = true
      end
    end

    def idle_reading
      @parser.on('--idle-reading=seconds', 'Amount of time channel can idle without incoming data') do |v|
        @options[:idle_reading] = v.to_i
      end
    end

    def idle_writing
      @parser.on('--idle-writing=seconds', 'Amount of time channel can idle without outgoing data') do |v|
        @options[:idle_writing] = v.to_i
      end
    end

    def log_requests
      @parser.on('-r', '--log-requests', 'Include individual request info in log output') do |v|
        @options[:log_requests] = v
      end
    end

    def web_root
      @parser.on('-w', '--web-root=path', 'Set the web root to a specific path') do |v|
        @options[:web_root] = v
      end
    end

    def verbose
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] ||= 0
        @options[:log_level] -= 1
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{::WebSocket::VERSION}"
        exit
      end
    end
  end
  # rubocop: enable Metrics/ClassLength
  # class ArgumentsParser

  def parse_arguments(arguments_parser = WebSocket::ArgumentsParser.new)
    arguments_parser.parser.parse!(ARGV)
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
    puts e.message
    puts parser
    exit
  rescue OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module WebSocket
