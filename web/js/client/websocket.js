
Client = function(host, port) {
  protocol = (window.location.protocol == 'https:') ? 'wss:' : 'ws:';
  host = host ? host : window.location.hostname;
  port = port ? port : (protocol == 'wss:' ? 443 : 4000);
  this.ws_uri = protocol + '//' + host + ':' + port + '/websocket';
  this.client = this;
  if (window.WebSocket) {
      this.WebSocket = window.WebSocket;
  } else if ('WebSocket' in window) {
      this.WebSocket = window.WebSocket;
  } else if (window.MozWebSocket) {
      this.WebSocket = window.MozWebSocket;
  } else if ('MozWebSocket' in window) {
      this.WebSocket = window.MozWebSocket;
  } else {
      this.WebSocket = window.MozWebSocket;
  }
};

Client.prototype.ws_uri = 'ws://localhost:4000/websocket';
Client.prototype.socket = null;
Client.prototype.WebSocket = null;
Client.prototype.history = new Array();
Client.prototype.index = 0;

Client.prototype.open = function(ws_uri) {
  this.ws_uri = ws_uri ? ws_uri : this.ws_uri;
  this.socket = new this.WebSocket(this.ws_uri);
  this.socket.onopen    = function(event) { console.log('WebSocket open!'); };
  this.socket.onclose   = function(event) { console.log('WebSocket closed.'); };
  this.socket.onmessage = function(event) { console.log(event.data); };
  this
};

Client.prototype.check = function() {
  console.log('Checking for WebSockets...');
  if (this.WebSocket) {
      console.log('Your browser supports WebSockets.');
  } else {
      console.log('Your browser does not support WebSockets yet.');
  }

  if (this.socket && this.socket.readyState == this.WebSocket.OPEN) {
      console.log('The WebSocket handshake with the server was successful.')
  } else {
      console.log('The WebSocket is not ready.')
  }

  return (this.socket != null && this.socket.readyState == this.WebSocket.OPEN);
};

Client.prototype.isConnected = function() {
   return (this.WebSocket && this.socket && this.socket.readyState == this.WebSocket.OPEN)
};

Client.prototype.close = function() {
  this.socket.close();
};

Client.prototype.send = function(message, record) {
  if (record != false) {
      if (this.index < this.history.length - 1) {
          this.history.splice(this.history.length - 1, 1);
          this.index = this.history.length - 1;
      }
      this.history.push(message);
      this.index = this.history.length - 1;
  }
  if (!this.WebSocket || !this.socket) { this.open(); }
  if (this.socket.readyState == this.WebSocket.OPEN) {
      this.socket.send(message);
  } else {
      console.log('The WebSocket is not ready yet!');
  }
};
